#include <stdio.h>
#include <stdint.h>

static const char *format = "{ \"sensor\": \"%s\", \"value\": %lf, \"timestamp\": %d }";

int format_json( float value,  uint32_t timestamp, uint8_t *sensor, uint8_t *buf)
{

  sprintf( buf, format, sensor, value, timestamp );

  return 0;
}
