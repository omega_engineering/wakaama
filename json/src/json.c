#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "json.h"
#include "printbuf.h"

static json_object *client_measurement;
static json_object *value_object;
static json_object *timestamp_object;


int init_json(void)
{

   client_measurement = json_object_new_object();
   value_object = json_object_new_double(1.0);
   timestamp_object = json_object_new_int(1);

   json_object_object_add( client_measurement, "value", value_object );
   json_object_object_add( client_measurement, "timestamp", timestamp_object );

   return 0;
}

int get_measurements_json( float f_val, int timestamp, char *buffer )
{

  fprintf(stdout, "JSON: value set to %lf\n", f_val );

  json_object *value_obj = json_object_object_get( client_measurement, "value" );
  json_object_set_double( value_obj, f_val );

  json_object *timestamp_obj = json_object_object_get( client_measurement, "timestamp" );
  json_object_set_int( timestamp_obj, timestamp );

  strcpy( buffer, json_object_to_json_string( client_measurement ) );

  return 0;
}


