#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <mosquitto.h>

#define MQTT_HOSTNAME "localhost"
#define MQTT_PORT  1883
#define MQTT_USERNAME "admin"
#define MQTT_PASSWORD "admin"
#define MQTT_BASE_TOPIC "lwm2mclient"

static struct mosquitto *mosq;

int init_mqtt( void )
{

    mosquitto_lib_init();
    mosq = mosquitto_new( NULL, true, NULL );

    if( !mosq )
    {
      fprintf(stderr, "Can't open mosquitto lib\n");
      return -1;
    }

    mosquitto_username_pw_set( mosq, MQTT_USERNAME, MQTT_PASSWORD );

    int ret = mosquitto_connect( mosq, MQTT_HOSTNAME, MQTT_PORT, 0 );

    if( ret )
    {
        fprintf( stderr, "Can't connect to mosquitto server\n" );
        return -1;
    }


return 0;
}


void cleanup_mqtt(void)
{

    mosquitto_disconnect( mosq );
    mosquitto_destroy( mosq );
    mosquitto_lib_cleanup();
}

int publish_mqtt( uint8_t *topic,  uint8_t *data, uint8_t data_len )
{
   int ret = mosquitto_publish( mosq, NULL, topic, data_len, data, 0, false );

   if( ret )
   {
       fprintf( stderr, "Can't publish to mosquitto server\n" );
       return -1;
   }
}
