#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define STRING_TYPE  0x00
#define INTEGER_TYPE 0x01
#define FLOAT_TYPE   0x02


static float value;
static unsigned int timestamp;
static uint8_t _name[100];

void get_tlv_values( float *val, unsigned int *ts, uint8_t *name )
{
  *val = value;
  *ts = timestamp;
  strncpy( name, _name, sizeof(_name) );
}


static void reverse( void *pin, int size )
{
  int n;
  uint8_t tmp;
  uint8_t *p = (uint8_t *)pin;

  for( n = 0; n < (size + 1)/2; n++ )
  {
    tmp = p[n];
    p[n] = p[size-1-n];
    p[size-1-n] = tmp;
  }
  
}

int parse_tlv( uint8_t *pu8_buf, uint8_t u8_len )
{
  uint8_t u32_index = 0;
  uint8_t u32_bytes_remaining = u8_len;
  uint32_t u32_length;
  uint8_t u8_type;
  uint8_t length_code;
  uint32_t int_val;
  float    float_val;

  u32_index += 3;

  while( u32_bytes_remaining )
  {
    // get length code

    length_code = pu8_buf[u32_index++];
    // get type
    u8_type = pu8_buf[u32_index++];
    
    // get length
    if( length_code  == 0xc8)
      u32_length = pu8_buf[u32_index++];
    else if( length_code  == 0xc4 )
      u32_length = 4;
    
    switch( u8_type )
    {
    case STRING_TYPE:
      memset( _name, 0, sizeof(_name));
      memcpy( _name, &pu8_buf[u32_index], u32_length );
      fprintf(stdout, "String:%s\n", _name );
      break;
      
    case INTEGER_TYPE:

      memcpy( &int_val, &pu8_buf[u32_index], 4 );
      reverse( &int_val, 4 );
      
      fprintf( stdout, "Integer: %08x\n", int_val );
      timestamp = int_val;
      break;
      
    case FLOAT_TYPE:
      memcpy( &float_val, &pu8_buf[u32_index], 4 );
      reverse( &float_val, 4 );
      
      value = float_val;

      fprintf( stdout, "Float: %lf\n", float_val );
      break;

    }

    u32_index += u32_length;

    // advance index and decrement remaining
    u32_bytes_remaining = u8_len - u32_index;
    
  }

  return 0;
}
